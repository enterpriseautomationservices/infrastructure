#!groovy

import hudson.util.*
import com.cloudbees.plugins.credentials.impl.*;
import com.cloudbees.plugins.credentials.*;
import com.cloudbees.plugins.credentials.domains.*;
import com.microsoft.jenkins.kubernetes.credentials.*;
import org.jenkinsci.plugins.plaincredentials.impl.*

Credentials c = (Credentials) new UsernamePasswordCredentialsImpl(CredentialsScope.GLOBAL, "int_bb", "bb creds", "integrations@enterpriseautomation.co.uk", "Renegade187!")
SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)
Credentials c2 = (Credentials) new KubeconfigCredentials(CredentialsScope.GLOBAL,'kube-prod-conf' ,'prod cluster config',new KubeconfigCredentials.FileOnMasterKubeconfigSource('/var/jenkins_home/.kube/config'))
SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c2)
Credentials c3 = (Credentials) new KubeconfigCredentials(CredentialsScope.GLOBAL,'kube-dev-conf' ,'dev cluster config',new KubeconfigCredentials.FileOnMasterKubeconfigSource('/var/jenkins_home/.kube/devconfig'))
SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c3)
Credentials c4 = (Credentials) new StringCredentialsImpl(CredentialsScope.GLOBAL,"prod-sa","prod cluster sa",Secret.fromString("kubeconfig-u-wdjz4.c-j2n9b:4thsrm77979k98g5fmxvg4mn9k7sbfvngxm4pbz8m2kz98lswc4994"))
SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c4)
Credentials c5 = (Credentials) new UsernamePasswordCredentialsImpl(CredentialsScope.GLOBAL, "jira_user", "jira jenkins user", "jenkins", "Renegade187!")
SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c5)